import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mystocks',
  templateUrl: './mystocks.page.html',
  styleUrls: ['./mystocks.page.scss'],
})
export class MystocksPage implements OnInit {

  acao: string;
  desc: string;
  preco: any;
  total: number;
  quant: number;
  o_acao: string;
  o_preco: any;
  o_total: number;
  o_quant: number;

  id_cliente: string;
  nome_cliente: string;
  saldo_cliente: number;


  constructor(
    private storage: Storage,
    private router: Router,
  ) {



  }

  ngOnInit() {

  }
  ionViewWillEnter() {


    this.storage.get('dados_stock_cliente').then((res2) => {
      if (res2 == null) {
        this.router.navigate(['/home']);

      } else {
        this.id_cliente = res2.id_cliente;
        this.nome_cliente = res2.nome_cliente;
        this.saldo_cliente = res2.saldo_cliente;


      }
    })


    this.storage.get('ordem_compra').then((res) => {
      if (res == null) {
        this.router.navigate(['/home']);

      } else {
        this.o_acao = res.acao;
        this.o_preco = res.preco;
        this.o_quant = res.quant;
        this.o_total = (res.preco *res.quant);


      }
    })
  }





  removeKey(key: string) {
    this.storage.remove(key).then(() => {
      console.log('removed ' + key);

    }).catch((error) => {
      console.log('removed error for ' + key + '', error);
    });
  }

  goPageHome(){
    this.router.navigate(['/home']);


  }
}
