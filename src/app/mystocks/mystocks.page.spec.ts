import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MystocksPage } from './mystocks.page';

describe('MystocksPage', () => {
  let component: MystocksPage;
  let fixture: ComponentFixture<MystocksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MystocksPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MystocksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
