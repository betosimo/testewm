import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MystocksPageRoutingModule } from './mystocks-routing.module';

import { MystocksPage } from './mystocks.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MystocksPageRoutingModule
  ],
  declarations: [MystocksPage]
})
export class MystocksPageModule {}
