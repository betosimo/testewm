import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  // @ViewChild("ion-searchbar", { read: ElementRef, static: false }) searchbarRef: ElementRef;
  list: any;
  filterList: any;
  searchTerm: string = '';
  id_cliente: string;
  nome_cliente: string;
  saldo_cliente: string;


  constructor(
    private storage: Storage,
    private router: Router,
  ) { }
  ngOnInit(): void {
  }
  ionViewWillEnter() {


    this.storage.get('dados_stock_cliente').then((res2) => {
      if (res2 == null) {
        this.id_cliente = "001";
    this.nome_cliente = "Fulano Comprador";
    this.saldo_cliente = "1000";

      } else {
        this.id_cliente = res2.id_cliente;
        this.nome_cliente = res2.nome_cliente;
        this.saldo_cliente = res2.saldo_cliente;


      }
    })

   

    this.list = [
      {
        "acao": "DASCAR PLUS LTDA",
        "desc": "Lorem ipsum dolor sit amet.",
        "preco": "17",

      }, {
        "acao": "ALIGATOR POOLS",
        "desc": "Duis eget arcu in ullamcorper.",
        "preco": "18",

      }, {
        "acao": "HOUSE ON WHEELS TRAILER PARK",
        "desc": "Duis hendrerit tincidunt.",
        "preco": "20",

      }, {
        "acao": "MIRABOLANTUS STARDUST LTDA",
        "desc": "Nullam posuere ultrices.",
        "preco": "12",

      },
    ];
    this.filterList = this.list;


    this.setFilteredStock();

  }




  setFilteredStock() {
    this.filterList = this.list.filter((stock) => {
      return (stock.acao.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1 || stock.desc.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
    });
  }

  setStock(acao) {
    let myStock = this.filterList.filter((stocks) => {
      return stocks.acao === acao;
    })

    this.storage.set('my_stock', myStock[0]);
    console.log(myStock[0]);
    let dados = {
      id_cliente: this.id_cliente,
      nome_cliente: this.nome_cliente,
      saldo_cliente: this.saldo_cliente,

    };


    this.storage.set('dados_stock_cliente', dados);
    this.router.navigate(['/stock']);
  }

  removeKey(key: string) {
    this.storage.remove(key).then(() => {
      console.log('removed ' + key);

    }).catch((error) => {
      console.log('removed error for ' + key + '', error);
    });
  }
  goPageMyStocks(){
    this.router.navigate(['/mystocks']);


  }


}

