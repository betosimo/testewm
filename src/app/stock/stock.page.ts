import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.page.html',
  styleUrls: ['./stock.page.scss'],
})
export class StockPage implements OnInit {

  acao: string;
  desc: string;
  preco: any;
  total: number;
  quant: number;

  id_cliente: string;
  nome_cliente: string;
  saldo_cliente: number;

  public ordemCompraForm: FormGroup;

  constructor(
    private storage: Storage,
    private router: Router,
    private formbuilder: FormBuilder,
  ) {

    this.ordemCompraForm = this.formbuilder.group({


      quantf: [null, [Validators.required]],


    })

  }

  ngOnInit() {

  }
  ionViewWillEnter() {
    this.storage.get('my_stock').then((res) => {
      if (res == null) {
        this.router.navigate(['/home']);

      } else {
        this.acao = res.acao;
        this.desc = res.desc;
        this.preco = res.preco;

      }
    })

    this.storage.get('dados_stock_cliente').then((res2) => {
      if (res2 == null) {
        this.router.navigate(['/home']);

      } else {
        this.id_cliente = res2.id_cliente;
        this.nome_cliente = res2.nome_cliente;
        this.saldo_cliente = res2.saldo_cliente;


      }
    })
  }

  onBlur() {


    this.total = this.preco * this.ordemCompraForm.value.quantf;
    console.log(this.total);
  }

  salvarOrdemCompra() {

    let dados = {
      acao: this.acao,
      preco: this.preco,
      quant: this.ordemCompraForm.value.quantf,

    };

    this.removeKey('my_stock');
    this.storage.set('ordem_compra', dados);
    setTimeout(() => {
      this.storage.get('ordem_compra').then((ordem) => {
        if (ordem != null) {

          let dados2 = {
            id_cliente: this.id_cliente,
            nome_cliente: this.nome_cliente,
            saldo_cliente: (this.saldo_cliente - this.total),
      
          };
      
      
          this.storage.set('dados_stock_cliente', dados2);
          this.saldo_cliente= dados2.saldo_cliente;
          setTimeout(() => {
          this.router.navigate(['/mystocks']);

        }, 1000);

        }

      })
    }, 1000);


  }

  removeKey(key: string) {
    this.storage.remove(key).then(() => {
      console.log('removed ' + key);

    }).catch((error) => {
      console.log('removed error for ' + key + '', error);
    });
  }

  goPageHome(){
    this.router.navigate(['/home']);


  }
}
